package com.example.compie.youtubeplaylistviewer.Data;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YoutubePlaylist {

    @SerializedName("ListTitle")
    @Expose
    private String listTitle;
    @SerializedName("ListItems")
    @Expose
    private List<YoutubeVideo> listItems = null;

    public String getListTitle() {
        return listTitle;
    }

    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    public List<YoutubeVideo> getListItems() {
        return listItems;
    }

    public void setListItems(List<YoutubeVideo> listItems) {
        this.listItems = listItems;
    }

}
package com.example.compie.youtubeplaylistviewer.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.compie.youtubeplaylistviewer.Data.YoutubePlaylist;
import com.example.compie.youtubeplaylistviewer.Presenter.PlaylistListPresenter;
import com.example.compie.youtubeplaylistviewer.R;
import com.example.compie.youtubeplaylistviewer.ViewHolders.YoutubePlaylistHolder;

import java.util.List;

/**
 * Created by compie on 09/01/18.
 */

public class PlaylistAdapter extends RecyclerView.Adapter<YoutubePlaylistHolder>{

    private final PlaylistListPresenter presenter;

    public PlaylistAdapter(PlaylistListPresenter playlistListPresenter){
        this.presenter = playlistListPresenter;
    }

    @Override
    public YoutubePlaylistHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        YoutubePlaylistHolder playlistHolder = new YoutubePlaylistHolder(LayoutInflater.from(parent.getContext()).
            inflate(R.layout.playlist_row, parent, false));

        return playlistHolder;
    }

    @Override
    public void onBindViewHolder(YoutubePlaylistHolder holder, int position) {

        presenter.onBindPlaylistRowViewAtPosition(position, holder);

    }

    @Override
    public int getItemCount() {
        return presenter.getPlaylistsCount();
    }


}

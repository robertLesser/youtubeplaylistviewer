package com.example.compie.youtubeplaylistviewer.Presenter;

import com.example.compie.youtubeplaylistviewer.Data.YoutubePlaylist;
import com.example.compie.youtubeplaylistviewer.Interface.PlaylistRowView;

import java.util.List;

/**
 * Created by compie on 09/01/18.
 */

public class PlaylistListPresenter {

    private final List<YoutubePlaylist> playlists;

    public PlaylistListPresenter(List<YoutubePlaylist> playlists) {
        this.playlists = playlists;
    }

    public void onBindPlaylistRowViewAtPosition(int position, PlaylistRowView rowView) {
        YoutubePlaylist playlist = playlists.get(position);
        rowView.setTitle(playlist.getListTitle());
    }

    public int getPlaylistsCount() {
        return playlists.size();
    }

}

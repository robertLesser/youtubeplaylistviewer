package com.example.compie.youtubeplaylistviewer.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.compie.youtubeplaylistviewer.Interface.PlaylistRowView;
import com.example.compie.youtubeplaylistviewer.R;

/**
 * Created by compie on 09/01/18.
 */

public class YoutubePlaylistHolder extends RecyclerView.ViewHolder implements PlaylistRowView {

    private TextView titleTextView;

    public YoutubePlaylistHolder(View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.playlist_title);
    }

    @Override
    public void setTitle(String title) {
        titleTextView.setText(title);
    }
}
